#include <iostream>
#include <string>
#include <Windows.h>
#include <direct.h>  
#include <stdlib.h>  
#include <stdio.h> 
#include <fstream> 

#define MAX_PATH 2048

using namespace std;

typedef char TCHAR;
typedef const char* LPCSTR;
typedef int (FAR PASCAL *RUNSCRIPT_FUNCTION)(int, LPCSTR);

LPCSTR pwdCommand();
void cdCommand(string path);
void createCommand(LPCSTR file);
void lsCommand();
int secret();
void exe(LPCSTR file);


int main()
{
	string cmd;
	while (true)
	{
		cout << ">>";
		getline(cin, cmd);

		if (cmd == "pwd")
		{
			cout << pwdCommand();
		}
		else if (cmd.substr(0, 2) == "cd")
		{
			cdCommand(cmd.substr(2, cmd.length() - 2));
		}
		else if (cmd.substr(0, 6) == "create")
		{
			cmd = cmd.substr(6, cmd.length() - 6);
			const char * newCmd = cmd.c_str();
			createCommand(newCmd);
		}
		else if (cmd == "ls")
		{
			lsCommand();
		}
		else if (cmd == "secert")
		{
			secret();
		}
		else if (cmd.substr(cmd.length() - 4, 4) == ".exe")
		{
			const char * newCmd = cmd.c_str();
			exe(newCmd);
		}
		cout << "\n";

	}
}

LPCSTR pwdCommand()
{
	TCHAR Buffer[MAX_PATH];
	DWORD dwRet;
	LPCSTR buffer;

	dwRet = GetCurrentDirectory(MAX_PATH, Buffer);
	buffer = _getcwd(nullptr, 0);
	if (dwRet == 0)
	{
		printf("GetCurrentDirectory failed (%d)\n", GetLastError());
		return "";
	}
	if (dwRet > MAX_PATH)
	{
		printf("Buffer too small; need %d characters\n", dwRet);
		return "";
	}

	else
	{
		return buffer;
	}
}

void cdCommand(string path)
{
	LPCSTR currPath = pwdCommand();
	TCHAR Buffer[MAX_PATH];
	if (!SetCurrentDirectory(currPath))
	{
		cout << "SetCurrentDirectory failed\n";
	}

	if (!SetCurrentDirectory(Buffer))
	{
		cout << "SetCurrentDirectory failed\n";
	}
}

void createCommand(LPCSTR file)
{
	HANDLE hFile = CreateFile(file, GENERIC_READ, FILE_SHARE_READ, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
	if (hFile != INVALID_HANDLE_VALUE)
	{
		CloseHandle(hFile);
	}
}
void lsCommand()
{
	LPCSTR currPath = pwdCommand();
	WIN32_FIND_DATA ffd;

	HANDLE hFind = FindFirstFile(TEXT(currPath), &ffd);

	if (INVALID_HANDLE_VALUE != hFind)
	{
		do
		{
			cout << ffd.cFileName;
		} while (FindNextFile(hFind, &ffd) != 0);
		FindClose(hFind);
	}
	else
	{
		cout << "There was a problem.\n";
	}
}
int secret()
{
	RUNSCRIPT_FUNCTION  RunScript;
	int  iReturn;

	// Load the DLL
	HINSTANCE hinstLib = LoadLibrary("secret.dll");
	if (hinstLib == NULL)
	{
		MessageBox(NULL, "Unable to load library", "Error", MB_OK | MB_ICONERROR);
		return 0;
	}

	// Get the function pointer
	RunScript = (RUNSCRIPT_FUNCTION)GetProcAddress(hinstLib, "RunScript");
	if (RunScript == NULL)
	{
		FreeLibrary(hinstLib);
		MessageBox(NULL, "Unable to load function", "Error", MB_OK | MB_ICONERROR);
		return 0;
	}

	// Call the function with '1' as a parameter
	iReturn = (*RunScript)(1, "Hello");

	// Unload the DLL
	FreeLibrary(hinstLib);

	return iReturn;
}
void exe(LPCSTR file)
{
	STARTUPINFO si = { sizeof(STARTUPINFO) };
	PROCESS_INFORMATION pi;

	if (!CreateProcess(file," /C avrdude.exe -c breakout -P ft0 -p m2560 -U flash:w:\"file.cpp.hex\":a",NULL, NULL, 0, 0, NULL, NULL, &si, &pi))
	{
		printf("CreateProcess failed (%d).\n", GetLastError());
	}
}